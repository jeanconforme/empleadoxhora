/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package empresapoo;

/**
 *
 * @author USUARIO
 */
public class EmpleadoxHora {
    private String nombre;
    private int edad;
    private String precio_hora_trabajada;
    private String num_horas_trabajadas;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getPrecio_hora_trabajada() {
        return precio_hora_trabajada;
    }

    public void setPrecio_hora_trabajada(String precio_hora_trabajada) {
        this.precio_hora_trabajada = precio_hora_trabajada;
    }

    public String getNum_horas_trabajadas() {
        return num_horas_trabajadas;
    }

    public void setNum_horas_trabajadas(String num_horas_trabajadas) {
        this.num_horas_trabajadas = num_horas_trabajadas;
    }
    
    
    
}
