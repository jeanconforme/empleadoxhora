/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package empresapoo;

/**
 *
 * @author USUARIO
 */
public class EmpleadoTemporal {

    private String nombre;
    private int edad;
    private String departamento;
    private String fecha_ingreso;
    private String salida_empresa;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getFecha_ingreso() {
        return fecha_ingreso;
    }

    public void setFecha_ingreso(String fecha_ingreso) {
        this.fecha_ingreso = fecha_ingreso;
    }

    public String getSalida_empresa() {
        return salida_empresa;
    }

    public void setSalida_empresa(String salida_empresa) {
        this.salida_empresa = salida_empresa;
    }}
